## Presentation
This is the git folder for the Philosthetic project lead by Guy Dammann in collaboration with Marie Dubremetz



## Prerequisites with Virtual env
If you use virtual env you can directly run these commands:
```bash
cd path/to/philosthetic
#Initiate your environment (only run the first time)
python3 -m venv .env
source .env/bin/activate 
pip install -r requirements.txt
#activate your environment
source .env/bin/activate 

```
## Content
All the scripts have been tested indepedently. Run them one by one in their folders without arguments.
For instance:
```bash
cd scraping_material
python scrape_archive.py
```
Only basic material for scraping and final work data have been loaded. They are an example of scraping and graphics for data about "beethoven sonata" on 2 databasis.

